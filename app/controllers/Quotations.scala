package controllers

import javax.inject.Inject
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.mvc._
import play.modules.reactivemongo._
import play.api.libs.json.Json
import reactivemongo.core.actors.Exceptions.PrimaryUnavailableException
import scala.concurrent.{ ExecutionContext, Future }

class Quotations extends abst.MongoAbstractController  {

  def quotationsModel = new models.QuotationModel(reactiveMongoApi)

  /*
  def index = Action {
    Ok(views.html.index("Qutations here"))
  }
*/

  def index = Action.async {implicit request =>
    quotationsModel.find()
      .map(posts => Ok(Json.toJson(posts.reverse)))
      .recover {case PrimaryUnavailableException => InternalServerError("Please install MongoDB")}
  }


}

