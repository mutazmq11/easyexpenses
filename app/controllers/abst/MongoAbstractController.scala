package controllers.abst

import play.api.Play.current
import play.api.mvc._
import play.modules.reactivemongo._


abstract class MongoAbstractController extends Controller with MongoController with ReactiveMongoComponents {

  lazy val reactiveMongoApi = current.injector.instanceOf[ReactiveMongoApi]

}

